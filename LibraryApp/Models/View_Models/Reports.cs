﻿using LibraryApp.Models.Data_Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryApp.Models.View_Models
{
    public class Reports
    {
        public ICollection<Report> List { get; set; }

        public Reports()
        {
            List = new List<Report>();
        }

        public Reports(ICollection<Report> newList)
        {
            List = newList;
        }
     
        public Report this[int i]
        {
            get => List.ElementAt(i);

            set => List.Add(value);
        }
    }
}