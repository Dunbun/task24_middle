﻿using LibraryApp.Models.Data_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryApp.Models.View_Models
{
    public class Articles
    {
        public ICollection<Article> List { get; set; }

        public Articles()
        {
            List = new List<Article>();
        }

        public Articles(ICollection<Article> newList)
        {
            List = newList;
        }


    }
}