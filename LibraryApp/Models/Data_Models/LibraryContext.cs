namespace LibraryApp.Models.Data_Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class LibraryContext : DbContext
    {
        public LibraryContext()
            : base("name=LibraryContext")
        {
        }

        public virtual DbSet<Report> Reports { get; set; }
        public virtual DbSet<Article> Articles { get; set; }
        public virtual DbSet<Questionary> Questionaries { get; set; }
    }
}