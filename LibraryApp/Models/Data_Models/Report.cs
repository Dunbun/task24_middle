﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryApp.Models.Data_Models
{
    public class Report
    {
        public int ReportId { get; set; }
        public string AuthorName { get; set; }
        public string Date { get; set; }
        
        public string ReportText { get; set; }

    }
}