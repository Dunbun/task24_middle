﻿using LibraryApp.Models.View_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryApp.Models.Data_Models
{
    public class DBWorker
    {
        public Articles GetArticles()
        {
            using (var db = new LibraryContext())
            {
                var query = db.Articles.Select(x => x).ToList();
                var articles = new Articles(query);

                return articles;
            }
        }

        public Reports GetReports()
        {
            using (var db = new LibraryContext())
            {
                var query = db.Reports.Select(x => x).ToList();
                var reports = new Reports(query);

                return reports;
            }
        }
        public void AddReport(Report report)
        {
            using (var db = new LibraryContext())
            {
                db.Reports.Add(report);
                db.SaveChanges();
            }
        }

        public void AddQuestionary(Questionary questionary)
        {
            using (var db = new LibraryContext())
            {
                db.Questionaries.Add(questionary);
                db.SaveChanges();
            }
        }
    }
}