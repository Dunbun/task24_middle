﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryApp.Models.Data_Models
{
    public class Questionary
    {
        public int QuestionaryId { get; set; }

        [Required(ErrorMessage = "Enter User email")]
        [StringLength(25, MinimumLength = 3)]
        public string UserEmail { get; set; }

        [Required(ErrorMessage = "Enter User name")]
        [StringLength(25, MinimumLength = 3)]
        public string UserName { get; set; }
        
        public string HTML { get; set; }
        
        public string CSS { get; set; }
        
        public string JS { get; set; }
        
        public string Gender { get; set; }
    }
}