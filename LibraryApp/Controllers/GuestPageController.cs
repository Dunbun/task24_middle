﻿using LibraryApp.Models;
using LibraryApp.Models.Data_Models;
using LibraryApp.Models.View_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryApp.Controllers
{
    public class GuestPageController : Controller
    {
        // GET: GuestPage
        public ActionResult Index()
        {
            var dbWorker = new DBWorker();
            var reports = dbWorker.GetReports();

            return View("Index", reports);
        }

        [HttpPost]
        public ActionResult AddReport(string authorName, string reportText )
        {   
            if (String.IsNullOrEmpty(authorName))
                ModelState.AddModelError("authorName", "Please enter your name");

            if (String.IsNullOrEmpty(reportText))
                ModelState.AddModelError("reportText", "Please enter report text");

            var dbWorker = new DBWorker();

            if (ModelState.IsValid)
            {   
                Report report = new Report() { AuthorName = authorName, ReportText = reportText, Date = DateTime.Now.ToString() };  
                dbWorker.AddReport(report);
            }

            var reports = dbWorker.GetReports();

            return View("Index", reports);
        }
    }
}